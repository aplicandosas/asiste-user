﻿using System;
using System.Windows.Forms;

namespace EncriptarDesencriptar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEncriptar_Click(object sender, EventArgs e)
        {
            txtResultado.Text = String.Empty;
            txtResultado.Text = Seguridad.Encriptar(txtTexto.Text);
        }

        private void btnDesencriptar_Click(object sender, EventArgs e)
        {
            txtResultado.Text = String.Empty;
            txtResultado.Text = Seguridad.DesEncriptar(txtTexto.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtTexto.Text = String.Empty;
            txtResultado.Text = String.Empty;
        }
    }

    public static class Seguridad
    {
        public static string Encriptar(this string _cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }
        public static string DesEncriptar(this string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            //result = System.Text.Encoding.Unicode.GetString(decryted, 0, decryted.ToArray().Length);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }
    }
}
