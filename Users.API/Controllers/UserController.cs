﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Users.API.Domains.Auth;
using Users.API.Infrastructure.DTO;
using Users.API.Infrastructure.Extensions;

namespace Users.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController : Controller
    {
        private AuthDomain _domain;
        private JwtGenerator _JwtGenerator;
        private readonly IConfiguration _Config;
        public UserController(IConfiguration config)
        {
            _domain = new AuthDomain(config);
            _JwtGenerator = new JwtGenerator(config);
            _Config = config;
        }

        /// <summary>
        /// Method to validate that the user exists in the database
        /// </summary>
        /// <param name="user">User, Email</param>
        /// <returns>Database Response</returns>
        [HttpGet("UserByEmail")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> IsValidUser(string user)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200, Message = "Validación Exitosa" };

            try
            {
                UserAuthRequest userRequest = new UserAuthRequest();
                UserSocialNetwork socialNetwork = new UserSocialNetwork();

                if (user != string.Empty)
                {
                    userRequest.UserName = user;

                    var usuarioCosultado = await _domain.IsValidUser(userRequest);
                    if (usuarioCosultado.Result != null)
                    {
                        socialNetwork.LoginSocialNetwork = Convert.ToInt32(usuarioCosultado.Result.Rows[0][0]) == 1 ? true : false;
                        socialNetwork.UserId = Convert.ToString(usuarioCosultado.Result.Rows[0][1]);
                        result.Data = socialNetwork;
                        return Ok(result);
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "Usuario no registrado";
                        result.StatusCode = 400;
                        result.Data = 0;
                        return Ok(result);
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "No se puede validar un usuario vacio.";
                    result.StatusCode = 400;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }           
        }

        /// <summary>
        /// Method to validate that the UserName exists in the database
        /// </summary>
        /// <param name="user">User, Email</param>
        /// <returns>Database Response</returns>
        [HttpGet("ExistUserName")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> IsValidExitsUserName(string user)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200, Message = "Validación Exitosa" };

            try
            {
                UserAuthRequest userRequest = new UserAuthRequest();
                UserSocialNetwork socialNetwork = new UserSocialNetwork();

                if (user != string.Empty)
                {
                    userRequest.UserName = user;

                    var usuarioCosultado = await _domain.IsValidExitsUserName(userRequest);
                    if (usuarioCosultado.Result != -999)
                    {
                        result.Data = usuarioCosultado.Result;
                        return Ok(result);
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "Usuario no registrado";
                        result.StatusCode = 400;
                        result.Data = 0;
                        return Ok(result);
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "No se puede validar un usuario vacio.";
                    result.StatusCode = 400;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Get the user by his id
        /// </summary>
        /// <param name="UserId">User Id</param>
        /// <returns>Database Response</returns>
        [HttpGet("UserById")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> GetUserById(string UserId)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200 };
            try
            {
                if (UserId != string.Empty)
                {
                    var usuarioCosultado = await _domain.GetUserById(UserId);
                    if (usuarioCosultado.Result != null) return Ok(usuarioCosultado); 
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "No se logró autenticar con el usuario";
                        result.StatusCode = 200;
                        return BadRequest(result);
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Usuario y contraseñas invalidos";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Get a list of users
        /// </summary>
        /// <returns>Database Response</returns>
        [HttpGet("Users")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> sp_GetUsersList()
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200, Message = "Consulta Exitosa" };
            try
            {
                List<AspNetUsers> aspNetUsers = new List<AspNetUsers>();
                aspNetUsers = await _domain.sp_GetUsersList();
                if (aspNetUsers.Count > 0) return Ok(aspNetUsers);
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Error al cargar los usuarios.";
                    result.StatusCode = 400;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Method for validation of the security code
        /// </summary>
        /// <param name="UserName">User, Email</param>
        /// <param name="SecurityStamp">Security code</param>
        /// <returns>Database Response</returns>
        [HttpGet("ValidateOTP")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> ValidateSecurityCode(string UserName, string SecurityStamp)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200, Data = SecurityStamp, Message = "Código Correcto" };

            try
            {
                UserAuthRequest userRequest = new UserAuthRequest();
               
                if (UserName != string.Empty && SecurityStamp != string.Empty)
                {
                    var codigoVerficado = await _domain.ValidateSecurityCode(UserName, SecurityStamp);
                    if (codigoVerficado.Result != false) return result;
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "Código invalido";
                        result.StatusCode = 200;
                        return BadRequest(result);
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Código invalido";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
            
        }

        /// <summary>
        /// User authentication method
        /// </summary>
        /// <param name="user">User Login</param>
        /// <param name="pass">Password Login</param>
        /// <returns>Database Response</returns>
        [HttpPost("Login")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> AuthUsers([FromBody]UserAuthRequest user)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200 };
            try
            {
                string token;

                if (user.UserName != string.Empty && user.Password != string.Empty)
                {
                    var usuarioCosultado = await _domain.UserAuthentication(user);
                    if (usuarioCosultado.Result != null)
                    {
                        token = _JwtGenerator.GenerateJwtToken();
                        usuarioCosultado.Result.AuthToken = token;                      
                        _domain.InsertToken(usuarioCosultado.Result);
                        return Ok(usuarioCosultado);
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "No se logró autenticar con el usuario";
                        result.StatusCode = 200;
                        return BadRequest(result);
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Usuario y contraseñas invalidos";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
            
        }

        /// <summary>
        /// Method to insert a new user
        /// </summary>
        /// <param name="UserName">User Login</param>
        /// <param name="NormalizedUserName">User Name</param>
        /// <param name="Email">Email</param>
        /// <param name="PasswordHash">Pasword encrit</param>
        /// <param name="PhoneNumber">telephone number</param>
        /// <returns>Database Response</returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> InsertUsers([FromBody]UserInsert user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200 };
                if (user.UserName != string.Empty && user.NormalizedUserName != string.Empty)
                {
                    var resultado = await _domain.InsertUsers(user.UserName, user.NormalizedUserName);
                    return Ok(resultado);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Usuario y contraseñas invalidos";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }                    
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method to upgrade a user
        /// </summary>
        /// <param name="Id">User ID</param>
        /// <param name="UserName">User Login</param>
        /// <param name="NormalizedUserName">User Name</param>
        /// <param name="Email">Email</param>
        /// <param name="PasswordHash">Pasword encrit</param>
        /// <param name="PhoneNumber">telephone number</param>
        /// <returns>Database Response</returns>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> UpdateUsers([FromBody] UserAuthRequest user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };

                if (user.UserName != string.Empty && user.Password != string.Empty)
                {
                    var resultado = await _domain.UpdateUsers(user.UserName, user.Password);
                    return Ok(result);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Usuario y contraseñas invalidos";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method to insert a new user
        /// </summary>
        /// <param name="user">Object New User</param>
        /// <returns>Database Response</returns>
        [HttpPost("UserInfo")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> InsertUserInfo([FromBody] AspNetUserInfo user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200 };
                if (user.UserName != string.Empty && user.NormalizedUserName != string.Empty)
                {
                    var resultado = await _domain.InsertUserInfo(user);
                    return Ok(resultado);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Usuario y contraseñas invalidos";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method to upgrade a user
        /// </summary>
        /// <param name="user">Object user</param>
        /// <returns>Database Response</returns>
        [HttpPut("UserInfo")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> UpdateUserInfo([FromBody] AspNetUserInfo user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };

                if (user.UserName != string.Empty)
                {
                    var resultado = await _domain.UpdateUserInfo(user);
                    return Ok(result);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Usuario y contraseñas invalidos";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method to upgrade a user
        /// </summary>
        /// <param name="user">Object user</param>
        /// <returns>Database Response</returns>
        [HttpPut("UserInfoPwd")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> UpdateUserPwd([FromBody] UserPwd user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };

                if (user.PasswordHash != string.Empty)
                {
                    var resultado = await _domain.UpdateUserInfoPwd(user);
                    return Ok(result);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Usuario y contraseñas invalidos";
                    result.StatusCode = 200;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Returns the users related to a role
        /// </summary>
        /// <param name="RoleName">Role Name</param>
        /// <returns>Database Response</returns>
        [HttpGet("UserByRol")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> UserByRol(string RoleName)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200, Message = "Consulta Exitosa" };
            try
            {
                List<AspNetUsers> aspNetUsers = new List<AspNetUsers>();
                aspNetUsers = await _domain.UserByRol(RoleName);
                if (aspNetUsers.Count > 0) return Ok(aspNetUsers);
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Error al cargar los usuarios.";
                    result.StatusCode = 400;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Returns the roles related to a user
        /// </summary>
        /// <param name="UserName">User Name</param>
        /// <returns>Database Response</returns>
        [HttpGet("RolByUser")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> RolByUser(string UserName)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200, Message = "Consulta Exitosa" };
            try
            {
                List<AspNetRoles> aspNetRoles = new List<AspNetRoles>();
                aspNetRoles = await _domain.RolByUser(UserName);
                if (aspNetRoles.Count > 0) return Ok(aspNetRoles);
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Error al cargar los roles.";
                    result.StatusCode = 400;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// The password of the user registered by the platform is obtained.
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns>Database Response</returns>
        [HttpGet("GetPasswordUser")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<dynamic>> GetPasswordUser(string email)
        {
            Result<dynamic> result = new Result<dynamic> { IsSuccess = true, StatusCode = 200, Message = "Consulta Exitosa" };
            try
            {
                if (email != string.Empty)
                {
                    result.Data = await _domain.GetPasswordUser(email);
                    if (result.Data != "Ok") result.StatusCode = 400;
                    return Ok(result);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "No se pudo enviar la contraseña";
                    result.StatusCode = 400;
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                result.StatusCode = 400;
                return BadRequest(result);
            }
        }

    }
}
