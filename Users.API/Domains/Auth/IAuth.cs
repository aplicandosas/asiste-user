﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Users.API.Infrastructure.DTO;

namespace Users.API.Domains.Auth
{
    public interface IAuth
    {
        Task<dynamic> UserAuthentication(UserAuthRequest userRequest);
        Task<dynamic> GetUserById(string pUserId);
        Task<dynamic> IsValidUser(UserAuthRequest userRequest);
        Task<dynamic> IsValidExitsUserName(UserAuthRequest pUsuario);
        Task<List<AspNetUsers>> sp_GetUsersList();
        void InsertToken(AspNetUsers aspNetUsers);
        Task<dynamic> InsertUsers(string UserName, string NormalizedUserName);
        Task<dynamic> UpdateUsers(string UserName, string PasswordHash);
        Task<dynamic> InsertUserInfo(AspNetUserInfo user);
        Task<dynamic> UpdateUserInfo(AspNetUserInfo user);
        Task<dynamic> UpdateUserInfoPwd(UserPwd user);
        Task<dynamic> ValidateSecurityCode(string UserName, string SecurityStamp);
        Task<List<AspNetRoles>> RolByUser(string UserName);
        Task<List<AspNetUsers>> UserByRol(string RoleName);
        Task<dynamic> GetPasswordUser(string email);

    }
}
