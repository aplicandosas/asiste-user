﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Users.API.Infrastructure.DTO;
using Users.API.Infrastructure.Extensions;
using Users.API.Infrastructure.Repositories;

namespace Users.API.Domains.Auth
{
    public class AuthDomain : IAuth
    {
        AuthRepository repository;
        public AuthDomain(IConfiguration config)
        {
            repository = new AuthRepository(config);
        }

        public async Task<dynamic> IsValidUser(UserAuthRequest userRequest)
        {
            try
            {
                return repository.IsValidUser(userRequest.UserName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> IsValidExitsUserName(UserAuthRequest userRequest)
        {
            try
            {
                return repository.IsValidExitsUserName(userRequest.UserName);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public async Task<dynamic> GetUserById(string pUserId)
        {
            try
            {
                return repository.GetUserById(pUserId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<AspNetUsers>> sp_GetUsersList()
        {
            try
            {
                return await repository.sp_GetUsersList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<dynamic> UserAuthentication(UserAuthRequest userRequest)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };
                var resultado = repository.UserAuthentication(userRequest.UserName, userRequest.Password);
                if (resultado == null)
                {
                    result.IsSuccess = false;
                    result.Message = "No se logró autenticar el usuario";
                    return result;
                }

                return resultado;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void InsertToken(AspNetUsers aspNetUsers)
        {
            try
            {
                AspNetUserTokens aspNetUserTokens = new AspNetUserTokens();
                aspNetUserTokens.UserId = aspNetUsers.Id;
                aspNetUserTokens.LoginProvider = aspNetUsers.UserName;
                aspNetUserTokens.Name = aspNetUsers.NormalizedUserName;
                aspNetUserTokens.Value = aspNetUsers.AuthToken;

                repository.InsertToken(aspNetUserTokens.UserId, aspNetUserTokens.LoginProvider, aspNetUserTokens.Name, aspNetUserTokens.Value);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> InsertUsers(string UserName, string NormalizedUserName)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };
                bool resultado = await repository.InsertUsers(UserName, NormalizedUserName);

                if (!resultado)
                {
                    result.IsSuccess = false;
                    result.Message = "No se logró crear el usuario, registro no insertado";
                    return result;
                }

                return resultado;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> UpdateUsers(string UserName, string PasswordHash)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };
                bool resultado = await repository.UpdateUsers(UserName, PasswordHash);

                if (!resultado)
                {
                    result.IsSuccess = false;
                    result.Message = "No se logró el cambio en el usuario, registro no actualizado";
                    return result;
                }

                return resultado;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> InsertUserInfo(AspNetUserInfo user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };
                bool resultado = await repository.InsertUserInfo(user);

                if (!resultado)
                {
                    result.IsSuccess = false;
                    result.Message = "No se logró crear el usuario, registro no insertado";
                    return result;
                }

                return resultado;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> UpdateUserInfo(AspNetUserInfo user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };
                bool resultado = await repository.UpdateUserInfo(user);

                if (!resultado)
                {
                    result.IsSuccess = false;
                    result.Message = "No se logró el cambio en el usuario, registro no actualizado";
                    return result;
                }

                return resultado;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> UpdateUserInfoPwd(UserPwd user)
        {
            try
            {
                Result<dynamic> result = new Result<dynamic> { IsSuccess = true };
                bool resultado = await repository.UpdateUserInfoPwd(user);

                if (!resultado)
                {
                    result.IsSuccess = false;
                    result.Message = "No se logró el actualizar la contrase;a en el usuario, registro no actualizado";
                    return result;
                }

                return resultado;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> ValidateSecurityCode(string UserName, string SecurityStamp)
        {
            try
            {
                return repository.ValidateSecurityCode(UserName, SecurityStamp);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<AspNetUsers>> UserByRol(string RoleName)
        {
            try
            {
                return await repository.UserByRol(RoleName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<AspNetRoles>> RolByUser(string UserName)
        {
            try
            {
                return await repository.RolByUser(UserName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> GetPasswordUser(string email)
        {
            try
            {
                return await repository.GetPasswordUser(email);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
