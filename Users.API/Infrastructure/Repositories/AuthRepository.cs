﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Users.API.Infrastructure.DBContext;
using Users.API.Infrastructure.DTO;
using Users.API.Infrastructure.Extensions;
using Users.API.Infrastructure.Services;

namespace Users.API.Infrastructure.Repositories
{
    public class AuthRepository
    {
        SqlDBClient Acceso;
        private readonly IConfiguration _config;
        private Generals _generals = new Generals();
        public AuthRepository(IConfiguration config)
        {
            _config = config;
        }

        public async Task<DataTable> IsValidUser(string pUsuario)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();

            try
            {
                DataTable datosFiltros = new DataTable();
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetValidUser", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserName", pUsuario);

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count != 0)
                    {
                        return datosFiltros;
                    }
                    else return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<int> IsValidExitsUserName(string pUsuario)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetValidExitsUserName", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserName", pUsuario);

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count != 0)
                    {
                        if (Convert.ToInt32(datosFiltros.Rows[0][0]) == 1)
                            return 1;
                        else return 0;
                    }
                    else return -999;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<AspNetUsers> GetUserById(string pUserId)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                AspNetUsers users = null;
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetUserById", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserId", pUserId);

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count > 0)
                    {
                        users = new AspNetUsers();
                        users.Id = datosFiltros.Rows[0][0].ToString();
                        users.UserName = datosFiltros.Rows[0][1].ToString();
                        users.NormalizedUserName = datosFiltros.Rows[0][2].ToString();
                        users.Email = datosFiltros.Rows[0][3].ToString();
                        users.NormalizedEmail = datosFiltros.Rows[0][4].ToString();
                        users.PasswordHash = datosFiltros.Rows[0][5].ToString();
                        users.SecurityStamp = datosFiltros.Rows[0][6].ToString();
                        users.EmailConfirmed = Convert.ToBoolean(datosFiltros.Rows[0][7]);
                        users.PhoneNumber = datosFiltros.Rows[0][8].ToString();
                        users.TwoFactorEnabled = Convert.ToBoolean(datosFiltros.Rows[0][9]);
                        users.LockoutEnabled = Convert.ToBoolean(datosFiltros.Rows[0][10]);
                        users.AccessFailedCount = Convert.ToInt32(datosFiltros.Rows[0][11]);
                        users.ResidenceCity = datosFiltros.Rows[0][12].ToString();
                        users.ExpeditionDocument = datosFiltros.Rows[0][13].ToString();
                        users.SwSocialNetworks = datosFiltros.Rows[0][14] == DBNull.Value ? false : Convert.ToBoolean(datosFiltros.Rows[0][14]);

                        users.DocumentType = datosFiltros.Rows[0][15] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][15].ToString();
                        users.DocumentNumber = datosFiltros.Rows[0][16] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][16].ToString();

                        users.aspNetRoles = new AspNetRoles()
                        {
                            Id = datosFiltros.Rows[0][17].ToString(),
                            Name = datosFiltros.Rows[0][18].ToString(),
                            NormalizedName = datosFiltros.Rows[0][19].ToString(),
                            ConcurrencyStamp = datosFiltros.Rows[0][20].ToString()
                        };
                        users.aspNetNetVehicles = new AspNetNetVehicles()
                        {
                            Id = datosFiltros.Rows[0][21].ToString(),
                            Type = datosFiltros.Rows[0][22].ToString(),
                            Model = datosFiltros.Rows[0][23].ToString(),
                            Color = datosFiltros.Rows[0][24].ToString(),
                            Plate = datosFiltros.Rows[0][25].ToString(),
                            Marck = datosFiltros.Rows[0][26].ToString(),
                            Line = datosFiltros.Rows[0][28].ToString()
                        };
                        users.aspNetCities = GetCitiesByUserId(datosFiltros.Rows[0][0].ToString());

                        return users;
                    }
                    else return users;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<List<AspNetUsers>> sp_GetUsersList()
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                List<AspNetUsers> listaUsuarios = new List<AspNetUsers>();
                AspNetUsers usuario = null;
                cn.Open();

                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetUsersList", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count != 0)
                    {
                        foreach (DataRow user in datosFiltros.Rows)
                        {
                            usuario = new AspNetUsers()
                            {
                                Id = user[0].ToString(),
                                UserName = user[1].ToString(),
                                NormalizedUserName = user[2].ToString(),
                                Email = user[3].ToString(),
                                NormalizedEmail = user[4].ToString(),
                                EmailConfirmed = Convert.ToBoolean(user[5]),
                                PasswordHash = user[6].ToString(),
                                SecurityStamp = user[7].ToString(),
                                PhoneNumber = user[8].ToString(),
                                TwoFactorEnabled = Convert.ToBoolean(user[9]),
                                LockoutEnabled = Convert.ToBoolean(user[10]),
                                AccessFailedCount = Convert.ToInt32(user[11]),
                                ResidenceCity = user[12].ToString(),
                                ExpeditionDocument = user[13].ToString(),
                                SwSocialNetworks = user[14] == DBNull.Value ? false : Convert.ToBoolean(user[14]),
                                DocumentType = datosFiltros.Rows[0][15] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][15].ToString(),
                                DocumentNumber = datosFiltros.Rows[0][16] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][16].ToString(),
                                aspNetRoles = new AspNetRoles()
                                {
                                    Id = user[17].ToString(),
                                    Name = user[18].ToString(),
                                    NormalizedName = user[19].ToString(),
                                    ConcurrencyStamp = user[20].ToString()
                                },
                                aspNetNetVehicles = new AspNetNetVehicles()
                                {
                                    Id = user[21].ToString(),
                                    Type = user[22].ToString(),
                                    Model = user[23].ToString(),
                                    Color = user[24].ToString(),
                                    Plate = user[25].ToString(),
                                    Marck = user[26].ToString(),
                                    Line = user[28].ToString()
                                },
                                aspNetCities = GetCitiesByUserId(user[0].ToString())
                            };
                            listaUsuarios.Add(usuario);
                        }
                        return listaUsuarios;
                    }
                    else return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<AspNetUsers> UserAuthentication(string pUsuario, string pClave)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                AspNetUsers users = null;
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetInfoUser", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserName", pUsuario);
                    cmdConsulta.Parameters.AddWithValue("@pPasswordHash ", Seguridad.Encriptar(pClave));

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count > 0)
                    {
                        users = new AspNetUsers();
                        users.Id = datosFiltros.Rows[0][0].ToString();
                        users.UserName = datosFiltros.Rows[0][1].ToString();
                        users.NormalizedUserName = datosFiltros.Rows[0][2].ToString();
                        users.Email = datosFiltros.Rows[0][3].ToString();
                        users.NormalizedEmail = datosFiltros.Rows[0][4].ToString();
                        users.PasswordHash = datosFiltros.Rows[0][5].ToString();
                        users.SecurityStamp = datosFiltros.Rows[0][6].ToString();
                        users.EmailConfirmed = Convert.ToBoolean(datosFiltros.Rows[0][7]);
                        users.PhoneNumber = datosFiltros.Rows[0][8].ToString();
                        users.TwoFactorEnabled = Convert.ToBoolean(datosFiltros.Rows[0][9]);
                        users.LockoutEnabled = Convert.ToBoolean(datosFiltros.Rows[0][10]);
                        users.AccessFailedCount = Convert.ToInt32(datosFiltros.Rows[0][11]);
                        users.ResidenceCity = datosFiltros.Rows[0][12].ToString();
                        users.ExpeditionDocument = datosFiltros.Rows[0][13].ToString();
                        users.SwSocialNetworks = datosFiltros.Rows[0][14] == DBNull.Value ? false : Convert.ToBoolean(datosFiltros.Rows[0][14]);

                        users.DocumentType = datosFiltros.Rows[0][15] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][15].ToString();
                        users.DocumentNumber = datosFiltros.Rows[0][16] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][16].ToString();

                        users.aspNetRoles = new AspNetRoles()
                        {
                            Id = datosFiltros.Rows[0][17].ToString(),
                            Name = datosFiltros.Rows[0][18].ToString(),
                            NormalizedName = datosFiltros.Rows[0][19].ToString(),
                            ConcurrencyStamp = datosFiltros.Rows[0][20].ToString()
                        };
                        users.aspNetNetVehicles = new AspNetNetVehicles()
                        {
                            Id = datosFiltros.Rows[0][21].ToString(),
                            Type = datosFiltros.Rows[0][22].ToString(),
                            Model = datosFiltros.Rows[0][23].ToString(),
                            Color = datosFiltros.Rows[0][24].ToString(),
                            Plate = datosFiltros.Rows[0][25].ToString(),
                            Marck = datosFiltros.Rows[0][26].ToString(),
                            Line = datosFiltros.Rows[0][28].ToString()
                        };
                        users.aspNetCities = GetCitiesByUserId(datosFiltros.Rows[0][0].ToString());

                        return users;
                    }
                    else return users;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void InsertToken(string pUserId, string pLoginProvider, string pName, string pValue)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_InsertToken", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserId", pUserId);
                    cmdConsulta.Parameters.AddWithValue("@pLoginProvider ", pLoginProvider);
                    cmdConsulta.Parameters.AddWithValue("@pName", pName);
                    cmdConsulta.Parameters.AddWithValue("@pValue ", pValue);
                    cmdConsulta.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<bool> InsertUsers(string UserName, string NormalizedUserName)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                EmailClient email = new EmailClient();
                Random rnd = new Random();
                string codigoSeguridad = rnd.Next(0, 999999).ToString();
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_InsertUsers", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pId", Guid.NewGuid().ToString("D"));
                    cmdConsulta.Parameters.AddWithValue("@pUserName", UserName);
                    cmdConsulta.Parameters.AddWithValue("@pNormalizedUserName", NormalizedUserName);
                    cmdConsulta.Parameters.AddWithValue("@pSecurityStamp", Seguridad.Encriptar($"{codigoSeguridad}"));
                    cmdConsulta.ExecuteNonQuery();
                }

                if (codigoSeguridad.Length == 5) codigoSeguridad = string.Concat("0", codigoSeguridad);
                await email.SendEmailAsync(NormalizedUserName, UserName, codigoSeguridad.ToString());
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<bool> UpdateUsers(string UserName, string PasswordHash)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_UpdateUsers", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserName", UserName);
                    cmdConsulta.Parameters.AddWithValue("@pPasswordHash", Seguridad.Encriptar(PasswordHash));
                    cmdConsulta.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<bool> InsertUserInfo(AspNetUserInfo user)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                EmailClient email = new EmailClient();
                Random rnd = new Random();
                string codigoSeguridad = rnd.Next(0, 999999).ToString();
                if (codigoSeguridad.Length == 5) codigoSeguridad = string.Concat("0", codigoSeguridad);

                string contraseña = string.Empty;

                if (user.SwSocialNetworks == true)
                    contraseña = Seguridad.Encriptar(_generals.GenerarContraseñaAleatorea());
                else
                {
                    if (user.Password == String.Empty)
                        contraseña = Seguridad.Encriptar(_generals.GenerarContraseñaAleatorea());
                    else
                        contraseña = Seguridad.Encriptar(user.Password);
                }

                cn.Open();
                string userId = Guid.NewGuid().ToString("D");

                using (SqlCommand cmdConsulta = new SqlCommand("sp_InsertUserInfo", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pId", userId);
                    cmdConsulta.Parameters.AddWithValue("@pUserName", user.UserName);
                    cmdConsulta.Parameters.AddWithValue("@pNormalizedUserName", user.NormalizedUserName);
                    cmdConsulta.Parameters.AddWithValue("@pEmail", user.Email);
                    cmdConsulta.Parameters.AddWithValue("@pPasswordHash", contraseña);
                    cmdConsulta.Parameters.AddWithValue("@pPhoneNumber", user.PhoneNumber);
                    cmdConsulta.Parameters.AddWithValue("@pSecurityStamp", user.SwSocialNetworks == false ? Seguridad.Encriptar($"{codigoSeguridad}") : String.Empty);
                    cmdConsulta.Parameters.AddWithValue("@pResidenceCity", user.ResidenceCity);
                    cmdConsulta.Parameters.AddWithValue("@pExpeditionDocument", user.ExpeditionDocument);
                    cmdConsulta.Parameters.AddWithValue("@pRoleId", user.aspNetRoles.Id);
                    cmdConsulta.Parameters.AddWithValue("@pSwSocialNetworks", user.SwSocialNetworks);
                    cmdConsulta.Parameters.AddWithValue("@pDocType", user.DocumentType);
                    cmdConsulta.Parameters.AddWithValue("@pDocNumber", user.DocumentNumber);

                    if (user.aspNetNetVehicles.Id != string.Empty)
                    {
                        cmdConsulta.Parameters.AddWithValue("@pVehicleId", Guid.NewGuid().ToString("D"));
                        cmdConsulta.Parameters.AddWithValue("@pType", user.aspNetNetVehicles.Type);
                        cmdConsulta.Parameters.AddWithValue("@pModel", user.aspNetNetVehicles.Model);
                        cmdConsulta.Parameters.AddWithValue("@pColor", user.aspNetNetVehicles.Color);
                        cmdConsulta.Parameters.AddWithValue("@pPlate", user.aspNetNetVehicles.Plate);
                        cmdConsulta.Parameters.AddWithValue("@pMarck", user.aspNetNetVehicles.Marck);
                        cmdConsulta.Parameters.AddWithValue("@pLine", user.aspNetNetVehicles.Line);
                    }

                    cmdConsulta.ExecuteNonQuery();
                    await InsertList(user.aspNetCities, userId);
                }

                if (user.SwSocialNetworks == false) await email.SendEmailAsync(user.NormalizedUserName, user.UserName, codigoSeguridad.ToString());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<bool> UpdateUserInfo(AspNetUserInfo user)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_UpdateUserInfo", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pId", user.Id);
                    cmdConsulta.Parameters.AddWithValue("@pUserName", user.UserName);
                    cmdConsulta.Parameters.AddWithValue("@pNormalizedUserName", user.NormalizedUserName);
                    cmdConsulta.Parameters.AddWithValue("@pEmail", user.Email);
                    //cmdConsulta.Parameters.AddWithValue("@pPasswordHash", Seguridad.Encriptar(user.Password));
                    cmdConsulta.Parameters.AddWithValue("@pPhoneNumber", user.PhoneNumber);
                    cmdConsulta.Parameters.AddWithValue("@pResidenceCity", user.ResidenceCity);
                    cmdConsulta.Parameters.AddWithValue("@pExpeditionDocument", user.ExpeditionDocument);
                    cmdConsulta.Parameters.AddWithValue("@pRoleId", user.aspNetRoles.Id);
                    cmdConsulta.Parameters.AddWithValue("@pSwSocialNetworks", user.SwSocialNetworks);
                    cmdConsulta.Parameters.AddWithValue("@pDocType", user.DocumentType);
                    cmdConsulta.Parameters.AddWithValue("@pDocNumber", user.DocumentNumber);
                    cmdConsulta.Parameters.AddWithValue("@pVehicleId", user.aspNetNetVehicles.Id);
                    cmdConsulta.Parameters.AddWithValue("@pType", user.aspNetNetVehicles.Type);
                    cmdConsulta.Parameters.AddWithValue("@pModel", user.aspNetNetVehicles.Model);
                    cmdConsulta.Parameters.AddWithValue("@pColor", user.aspNetNetVehicles.Color);
                    cmdConsulta.Parameters.AddWithValue("@pPlate", user.aspNetNetVehicles.Plate);
                    cmdConsulta.Parameters.AddWithValue("@pMarck", user.aspNetNetVehicles.Marck);
                    cmdConsulta.Parameters.AddWithValue("@pLine", user.aspNetNetVehicles.Line);

                    cmdConsulta.ExecuteNonQuery();
                    await EliminarListCities(user.Id);
                    await InsertList(user.aspNetCities, user.Id);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<bool> UpdateUserInfoPwd(UserPwd user)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_UpdateUserInfoPwd", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pId", user.Id);
                    cmdConsulta.Parameters.AddWithValue("@pPasswordHash", Seguridad.Encriptar(user.PasswordHash));
                    cmdConsulta.ExecuteNonQuery();
                   
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<bool> ValidateSecurityCode(string UserName, string SecurityStamp)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_ValidateSecurityCode", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserName", UserName);
                    cmdConsulta.Parameters.AddWithValue("@pSecurityStamp", Seguridad.Encriptar(SecurityStamp));
                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count != 0)
                    {
                        if (Convert.ToInt32(datosFiltros.Rows[0][0]) == 1)
                            return true;
                    }
                    else return false;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<List<AspNetRoles>> RolByUser(string UserName)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                List<AspNetRoles> listaRoles = new List<AspNetRoles>();
                AspNetRoles rol = null;
                cn.Open();

                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetRolByUser", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserName", UserName);

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count != 0)
                    {
                        foreach (DataRow role in datosFiltros.Rows)
                        {
                            rol = new AspNetRoles()
                            {
                                Id = role[0].ToString(),
                                Name = role[1].ToString(),
                                NormalizedName = role[2].ToString(),
                                ConcurrencyStamp = role[3].ToString(),
                            };
                            listaRoles.Add(rol);
                        }
                        return listaRoles;
                    }
                    else return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public async Task<List<AspNetUsers>> UserByRol(string RoleName)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                List<AspNetUsers> listaUsuarios = new List<AspNetUsers>();
                AspNetUsers usuario = null;
                cn.Open();

                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetUserByRol", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pRolName", RoleName);

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count != 0)
                    {
                        foreach (DataRow user in datosFiltros.Rows)
                        {
                            usuario = new AspNetUsers()
                            {
                                Id = user[0].ToString(),
                                UserName = user[1].ToString(),
                                NormalizedUserName = user[2].ToString(),
                                Email = user[3].ToString(),
                                NormalizedEmail = user[4].ToString(),
                                EmailConfirmed = Convert.ToBoolean(user[5]),
                                PasswordHash = user[6].ToString(),
                                SecurityStamp = user[7].ToString(),
                                PhoneNumber = user[8].ToString(),
                                TwoFactorEnabled = Convert.ToBoolean(user[9]),
                                LockoutEnabled = Convert.ToBoolean(user[10]),
                                AccessFailedCount = Convert.ToInt32(user[11]),
                                ResidenceCity = user[12].ToString(),
                                ExpeditionDocument = user[13].ToString(),
                                SwSocialNetworks = user[14] == DBNull.Value ? false : Convert.ToBoolean(user[14]),
                                DocumentType = datosFiltros.Rows[0][15] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][15].ToString(),
                                DocumentNumber = datosFiltros.Rows[0][16] == DBNull.Value ? String.Empty : datosFiltros.Rows[0][16].ToString(),
                                aspNetRoles = new AspNetRoles()
                                {
                                    Id = user[17].ToString(),
                                    Name = user[18].ToString(),
                                    NormalizedName = user[19].ToString(),
                                    ConcurrencyStamp = user[20].ToString()
                                },
                                aspNetNetVehicles = new AspNetNetVehicles()
                                {
                                    Id = user[21].ToString(),
                                    Type = user[22].ToString(),
                                    Model = user[23].ToString(),
                                    Color = user[24].ToString(),
                                    Plate = user[25].ToString(),
                                    Marck = user[26].ToString(),
                                    Line = user[28].ToString()
                                },
                                aspNetCities = GetCitiesByUserId(user[0].ToString())
                            };
                            listaUsuarios.Add(usuario);
                        }
                        return listaUsuarios;
                    }
                    else return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }


        public async Task<string> GetPasswordUser(string email)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                EmailClient objEmail = new EmailClient();
                string passw = string.Empty;
                cn.Open();

                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetPasswordUser", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pEmailUser", email);

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count != 0)
                    {
                        await objEmail.SendPasswordEmailAsync(datosFiltros.Rows[0][1].ToString(), email, Seguridad.DesEncriptar(datosFiltros.Rows[0][0].ToString()));
                        return "Ok";
                    }
                    else return "No se encontraron resultados.";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<AspNetCities> GetCitiesByUserId(string pUserId)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                AspNetCities cities = null;
                List<AspNetCities> listaCities = new List<AspNetCities>();
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("sp_GetCitiesBbyUserId", cn))
                {
                    cmdConsulta.CommandType = CommandType.StoredProcedure;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@pUserId", pUserId);

                    SqlDataAdapter daConsulta = new SqlDataAdapter(cmdConsulta);
                    daConsulta.Fill(datosFiltros);

                    if (datosFiltros.Rows.Count > 0)
                    {
                        for (int i = 0; i < datosFiltros.Rows.Count; i++)
                        {
                            cities = new AspNetCities();
                            cities.Id = datosFiltros.Rows[i][0].ToString();
                            cities.CityName = datosFiltros.Rows[i][1].ToString();
                            cities.UserId = datosFiltros.Rows[i][2].ToString();
                            listaCities.Add(cities);
                        }

                        return listaCities;
                    }
                    else return listaCities;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }


        private async Task InsertList(IEnumerable<AspNetCities> citiesList, string userId)
        {
            try
            {
                DynamicParameters tempParams = new DynamicParameters();
                using (var connection = new SqlConnection(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"])))
                {
                    var parameters = citiesList.Select(u =>
                    {
                        tempParams.Add("@city_Id", Guid.NewGuid().ToString("D"), DbType.String, ParameterDirection.Input);
                        tempParams.Add("@name", u.CityName, DbType.String, ParameterDirection.Input);
                        tempParams.Add("@userid", userId, DbType.String, ParameterDirection.Input);
                        return tempParams;
                    });

                    await connection.ExecuteAsync("INSERT INTO [AspNetCities] (Id, CityName, UserId) VALUES (@city_Id, @name, @userid)",
                        parameters).ConfigureAwait(false);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<bool> EliminarListCities(string userId)
        {
            Acceso = new SqlDBClient(Seguridad.DesEncriptar(_config["SQLConection:DBConnection"]));
            SqlConnection cn = Acceso.getConnection();
            try
            {
                DataTable datosFiltros = new DataTable();
                cn.Open();
                using (SqlCommand cmdConsulta = new SqlCommand("DELETE FROM [AspNetCities] WHERE UserId = @userid", cn))
                {
                    cmdConsulta.CommandType = CommandType.Text;
                    cmdConsulta.Parameters.Clear();
                    cmdConsulta.Parameters.AddWithValue("@userid", userId);
                    cmdConsulta.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }


    }
}
