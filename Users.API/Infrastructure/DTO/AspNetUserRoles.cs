﻿namespace Users.API.Infrastructure.DTO
{
	public class AspNetUserRoles
	{
		public string UserId { get; set; }
		public string RoleId { get; set; }
	}
}
