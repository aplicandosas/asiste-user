﻿namespace Users.API.Infrastructure.DTO
{
	public class AspNetRoles
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string NormalizedName { get; set; }
		public string ConcurrencyStamp { get; set; }
	}
}
