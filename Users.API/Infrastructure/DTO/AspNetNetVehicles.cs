﻿namespace Users.API.Infrastructure.DTO
{
	public class AspNetNetVehicles
	{
		public string Id { get; set; }
		public string Type { get; set; }
		public string Model { get; set; }
		public string Color { get; set; }
		public string Plate { get; set; }
		public string Marck { get; set; }
		public string Line { get; set; }
	}
}
