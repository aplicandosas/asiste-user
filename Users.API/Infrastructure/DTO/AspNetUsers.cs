﻿using System;
using System.Collections.Generic;

namespace Users.API.Infrastructure.DTO
{
	public class AspNetUsers
	{
		public string Id { get; set; }
		public string UserName { get; set; }
		public string NormalizedUserName { get; set; }
		public string Email { get; set; }
		public string NormalizedEmail { get; set; }
		public bool EmailConfirmed { get; set; }
		public string PasswordHash { get; set; }
		public string SecurityStamp { get; set; }
		public string ConcurrencyStamp { get; set; }
		public string PhoneNumber { get; set; }
		public bool PhoneNumberConfirmed { get; set; }
		public bool TwoFactorEnabled { get; set; }
		public DateTime LockoutEnd { get; set; }
		public bool LockoutEnabled { get; set; }
		public int AccessFailedCount { get; set; }
		public string AuthToken { get; set; }
		public string ResidenceCity { get; set; }		
		public string ExpeditionDocument { get; set; }
		public bool SwSocialNetworks { get; set; }
		public string DocumentType { get; set; }
		public string DocumentNumber { get; set; }
		public AspNetRoles aspNetRoles { get; set; }
		public AspNetNetVehicles aspNetNetVehicles { get; set; }
		public IEnumerable<AspNetCities> aspNetCities { get; set; }
	}
}
