﻿namespace Users.API.Infrastructure.DTO
{
	public class UserAuthRequest
	{
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
