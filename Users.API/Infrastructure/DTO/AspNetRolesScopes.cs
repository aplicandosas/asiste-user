﻿
namespace Users.API.Infrastructure.DTO
{
	public class AspNetRolesScopes
	{
		public string RoleId { get; set; }
		public string ScopeId { get; set; }
	}
}
