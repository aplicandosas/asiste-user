﻿namespace Users.API.Infrastructure.DTO
{
    public class UserInsert
    {
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
    }
}
