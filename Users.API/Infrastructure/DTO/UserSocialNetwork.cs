﻿namespace Users.API.Infrastructure.DTO
{
    public class UserSocialNetwork
    {
        public bool LoginSocialNetwork { get; set; }
        public string UserId { get; set; }
    }
}
