﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Users.API.Infrastructure.DTO
{
    public class UserPwd
    {
        public string Id { get; set; }

        public string PasswordHash { get; set; }
    }
}
