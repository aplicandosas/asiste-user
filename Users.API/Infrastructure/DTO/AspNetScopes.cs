﻿
namespace Users.API.Infrastructure.DTO
{
	public class AspNetScopes
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string ScopeType { get; set; }
		public bool ScopeValue { get; set; }
	}
}
