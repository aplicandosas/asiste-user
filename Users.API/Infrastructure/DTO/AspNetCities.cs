﻿namespace Users.API.Infrastructure.DTO
{
	public class AspNetCities
	{
		public string Id { get; set; }
		public string CityName { get; set; }
		public string UserId { get; set; }
	}
}
