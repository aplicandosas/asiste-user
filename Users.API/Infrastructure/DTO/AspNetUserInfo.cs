﻿
using System.Collections.Generic;

namespace Users.API.Infrastructure.DTO
{
    public class AspNetUserInfo
	{
		public string Id { get; set; }
		public string UserName { get; set; }
		public string NormalizedUserName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string PhoneNumber { get; set; }
		public string ResidenceCity { get; set; }
		public string ExpeditionDocument { get; set; }
		public bool SwSocialNetworks { get; set; }
		public string DocumentType { get; set; }
		public string DocumentNumber { get; set; }
		public AspNetRoles aspNetRoles { get; set; }
		public AspNetNetVehicles aspNetNetVehicles { get; set; }
		public IEnumerable<AspNetCities> aspNetCities { get; set; }
	}
}
