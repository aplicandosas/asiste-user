﻿using Microsoft.Extensions.Options;
using System;
using System.Reflection;

namespace Users.API.Infrastructure.Extensions
{
    public class AppSettingGlobal
    {
        private AppSetting ListSettings { get; set; }
        public AppSettingGlobal(IOptions<AppSetting> settings)
        {
            ListSettings = settings.Value;
        }

        public string GetValue(string Key)
        {
            try
            {
                var properties = ListSettings.GetType();
                PropertyInfo value = properties.GetProperty(Key);
                return value.GetValue(ListSettings, null).ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
