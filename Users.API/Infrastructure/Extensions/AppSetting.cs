﻿namespace Users.API.Infrastructure.Extensions
{
    public class AppSetting
    {
        public string SecretKeyAuth { get; set; }
        public string DBConnection { get; set; }
        public string ClientIdAuth { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string MessageAuthSuccess { get; set; }
        public string MessageAuthNotValid { get; set; }
        public string MessageSuccessToken { get; set; }
        public string Separator { get; set; }
    }
}
