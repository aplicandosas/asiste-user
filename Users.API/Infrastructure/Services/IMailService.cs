﻿using System.Threading.Tasks;

namespace Users.API.Infrastructure.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(string nombre, string emailData, string codigo);
    }
}
