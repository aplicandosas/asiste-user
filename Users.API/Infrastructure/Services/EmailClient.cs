﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Threading.Tasks;

namespace Users.API.Infrastructure.Services
{
    public class EmailClient : IMailService
    {
        public async Task SendEmailAsync(string nombre, string emailDatam, string codigo)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("Asiste info", "juan@aplicando.co"));
                message.To.Add(new MailboxAddress("Usuario", emailDatam));
                message.Subject = "Código de verificación Asiste...";
                message.Body = new TextPart("plain")
                {
                    Text = $"Hola, {nombre}." +
                    $"\n Tú código de verificación para el ingreso a la plataforma de Asiste es: {codigo}" +
                    $"\n Ingreselo en la plataforma para poder continuar."
                };

                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync("smtp.zoho.com", 465, true);
                    await client.AuthenticateAsync("juan@aplicando.co", "Getcom2018**");
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task SendPasswordEmailAsync(string nombre, string emailDatam, string passw)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("Asiste info", "juan@aplicando.co"));
                message.To.Add(new MailboxAddress("Usuario", emailDatam));
                message.Subject = "Tu contraseña para ingresar a Asiste...";
                message.Body = new TextPart("plain")
                {
                    Text = $"Hola, {nombre}." +
                    $"\n\nTú contraseña para el ingreso a la plataforma de Asiste es: {passw}" +
                    $"\nYa puedes ingresar con confianza. "+
                    $"\n\nEste es un servicio del Grupo de Tecnología de Asiste." +
                    $"\n \n"
                };

                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync("smtp.zoho.com", 465, true);
                    await client.AuthenticateAsync("juan@aplicando.co", "Getcom2018**");
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
